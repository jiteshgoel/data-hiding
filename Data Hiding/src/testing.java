import java.awt.Point;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;
//import java.util.Scanner;
import javax.imageio.ImageIO;
public class testing 
{
	 private static SampleModel sampleModel;
	 public static int[][] image_to_matrix(File file) throws IOException
	 {
		 try
		 {
		 	BufferedImage img = ImageIO.read(file);
		 	Raster raster=img.getData();
		 	int w=raster.getWidth(),h=raster.getHeight();
		 	sampleModel = raster.getSampleModel();
		 	int pixels[][]=new int[w][h];
		 	for(int x=0;x<w;x++)
		 	{
			 	for(int y=0;y<h;y++)
		     	{
				 	pixels[x][y]=raster.getSample(x,y,0);
		     	}
		 	}
		 	System.out.println("Done"+w+"		"+h);
		 	return pixels;
		 }
	 	catch (Exception e)
	 	{
		     e.printStackTrace();
		}
		 return null;
	 }
	 public static java.awt.Image matrix_to_image(int pixels[][],String name) throws IOException
	 {
		 int w=pixels.length;
	     int h=pixels[0].length;
	     BufferedImage image=new BufferedImage(w,h,BufferedImage.TYPE_BYTE_GRAY);
	     WritableRaster raster= Raster.createWritableRaster(sampleModel, new Point(0,0));
	     for(int i=0;i<w;i++)
	     {
	         for(int j=0;j<h;j++)
	         {
	             raster.setSample(i,j,0,pixels[i][j]);
	         }
	     }
	     image.setData(raster);
	     File output=new File(name);
	     try 
	     {
	    	 	ImageIO.write(image,"jpg",output);
	     }
	     catch (Exception e)
	     {
	    	 e.printStackTrace();
	     }
	     System.out.println(name+"	Image generated");
	     return image;
	}
	 public static void main(String args[]) throws IOException
	 {
		 int pixels[][]=new int[2000][2000];
		 int w,h,i,j,k,start,inc;
		 pixels=image_to_matrix(new File("D:/Users/user/workspace/Java Project/Sample Images/woman.jpg"));
		 matrix_to_image(pixels,"Input-image.jpg");
		 //System.out.println("Image genrated");
		 w=pixels.length;
	     h=pixels[0].length;
		 int locmap[][]=new int[w][h];
		 int out_pixels[][]=new int[w][h];
		 for(i=0;i<w;i++)
			 for(j=0;j<h;j++)
				 out_pixels[i][j]=pixels[i][j];
		 //out_pixels=pixels;
		 int difference[][]=new int[w][h];
		 /*for(i=0;i<w;i++)
		 {
			 for(j=0;j<h;j++)
				 System.out.print(out_pixels[i][j]+" ");
			 System.out.println();
		 }*/
		 //int segnum;
		 int hist[]=new int[256];
		 //System.out.println(hist[185]);
		 for(i=0;i<w;i++)
			 for(j=0;j<h;j++)
				 hist[pixels[i][j]]++;
		 for(i=0;i<256;i++)
		 {
				System.out.print(hist[i]+" ");
				//sum+=hist[i];
		 }
		 String secret="10101110011111110010100110000100100001111110101010110110011";
		 int secret_ptr=0;
		 
		 //Algorithm Starts.
		 int pwr[]={1,2,4,8,16,32,64,128,256};
		 for(int l=0;l<8;l++)
		 {
			 int intensities_per_seg=256/pwr[l];
			 int code_map[]=new int[intensities_per_seg];
			 int seg_peaks[]=new int[pwr[l]];
			 int first_occur[]=new int[pwr[l]];
			 for(i=0;i<pwr[l];i++)
			 {
				 int max=hist[i*intensities_per_seg];
				 int val=i*intensities_per_seg;
				 for(j=i*intensities_per_seg+1;j<(i+1)*intensities_per_seg;j++)
				 {
					 if(hist[j]>max)
					 {
						 max=hist[j];
						 val=j;
					 }
				 }
				 seg_peaks[i]=val;
			 }
			 for(i=0;i<intensities_per_seg;i++)
				 code_map[i]=-1;
			 Double temp=Math.log(intensities_per_seg)/Math.log(2);
			 int bits=temp.intValue();
			 System.out.println("\n"+intensities_per_seg+"			"+bits);
			 for(i=0;i<intensities_per_seg;i++)
			 {
				 Random generator=new Random();
				 j=generator.nextInt(intensities_per_seg);
				 while(code_map[j]!=-1)
					 j=(j+1)%intensities_per_seg;
				 code_map[j]=i;
			 }
			 System.out.print("code_map-->");
			 for(i=0;i<intensities_per_seg;i++)
				 System.out.print(code_map[i]+" ");
			 System.out.print("\n\nSegment Peaks-->");
			 for(i=0;i<pwr[l];i++)
				 System.out.print(seg_peaks[i]+" ");
			 for(i=0;i<w;i++)
			 {
				 if(i%2==0)
				 {
					 start=0;
					 inc=1;
				 }
				 else
				 {
					 start=h-1;
					 inc=-1;
				 }
				 for(j=start;;j=j+inc)
				 {
					 if(i%2==0)
					 {
						 if(j>=h)
							 break;
					 }
					 else
					 {
						 if(j<=0)
							 break;
					 }
					 int pix=pixels[i][j];
					 int seg=-1;
					 for(k=0;k<pwr[l];k++)
					 {
						 if(seg_peaks[k]==pix)
							 seg=k;
					 }
					 if(seg==-1)
						 continue;
					 locmap[i][j]=1;
					 if(first_occur[seg]==0)
					 { 
						 first_occur[seg]=1;
						 continue;
					 }
					int cur_bits=bits-1;
					int dec_num=0;
					while(cur_bits!=-1)
					{
						int ch=(secret.charAt(secret_ptr)-'0');
						dec_num=dec_num+ch*pwr[cur_bits];
						cur_bits--;
						secret_ptr=((secret_ptr+1)%(secret.length()));
					}
					int new_pix=-1;
					for(k=0;k<intensities_per_seg;k++)
					{
						if(code_map[k]==dec_num)
						{
							new_pix=seg*intensities_per_seg+k;
							out_pixels[i][j]=new_pix;
							break;
						}
					}
				 }	 
			 }
			 matrix_to_image(out_pixels,"output"+l+".jpg");
			 for(i=0;i<w;i++)
			 {
				 for(j=0;j<h;j++)
				 {
					 difference[i][j]=out_pixels[i][j]-pixels[i][j];
					 //System.out.print(difference[i][j]+" ");
				 }
			 }
			 matrix_to_image(difference,"diffmat"+l+".jpg");
			 System.out.println("done");
			
		 }
	}
}