import java.awt.Point;
import java.awt.image.*;
import java.io.*;
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.PrintWriter;
import java.util.Random;
//import java.util.Scanner;
import javax.imageio.ImageIO;
public class data_hiding 
{
	 private static SampleModel sampleModel;
	 public static int[][] image_to_matrix(File file) throws IOException
	 {
		 try
		 {
		 	BufferedImage img = ImageIO.read(file);
		 	Raster raster=img.getData();
		 	int w=raster.getWidth(),h=raster.getHeight();
		 	sampleModel = raster.getSampleModel();
		 	int pixels[][]=new int[h][w];
		 	for(int x=0;x<h;x++)
		 	{
			 	for(int y=0;y<w;y++)
		     	{
				 	pixels[x][y]=raster.getSample(y,x,0);
		     	}
		 	}
		 	//System.out.println("Done	"+h+"		"+w);
		 	return pixels;
		 }
	 	catch (Exception e)
	 	{
		     e.printStackTrace();
		}
		 return null;
	 }
	 public static java.awt.Image matrix_to_image(int pixels[][],String name) throws IOException
	 {
		 int h=pixels.length;
	     int w=pixels[0].length;
	     BufferedImage image=new BufferedImage(w,h,BufferedImage.TYPE_BYTE_GRAY);
	     WritableRaster raster= Raster.createWritableRaster(sampleModel, new Point(0,0));
	     for(int i=0;i<h;i++)
	     {
	         for(int j=0;j<w;j++)
	         {
	             raster.setSample(j,i,0,pixels[i][j]);
	         }
	     }
	     image.setData(raster);
	     File output=new File(name);
	     try 
	     {
	    	 	ImageIO.write(image,"jpg",output);
	     }
	     catch (Exception e)
	     {
	    	 e.printStackTrace();
	     }
	     System.out.println(name+"	Image generated");
	     return image;
	}
	 public static void main(String args[]) throws IOException
	 {
		 //System.out.println("fgffdbgbbsgbsgbsgb");
		 int pixels[][]=new int[2000][2000];
		 int w,h,i,j,k,start,inc;
		 pixels=image_to_matrix(new File("1.jpg"));
		 matrix_to_image(pixels,"Input-image.jpg");
		 h=pixels.length;
	     w=pixels[0].length;
		 int locmap[][]=new int[h][w];
		 int out_pixels[][]=new int[h][w];
		 for(i=0;i<h;i++)
			 for(j=0;j<w;j++)
				 out_pixels[i][j]=pixels[i][j];
		 int difference[][]=new int[h][w];
		 /*for(i=0;i<h;i++)
		 {
			 for(j=0;j<w;j++)
				 System.out.print(out_pixels[i][j]+" ");
			 System.out.println();
		 }*/
		 int hist[]=new int[256];
		 for(i=0;i<h;i++)
			 for(j=0;j<w;j++)
				 hist[pixels[i][j]]++;
		  BufferedReader in = new BufferedReader(new FileReader("code_to_hide.txt"));
		  int codelength=0;
		  //Algorithm Starts.
		 int pwr[]={1,2,4,8,16,32,64,128,256};
		 int segnum = 128;
		 int intensities_per_seg=256/segnum;
		 int code_map[]=new int[intensities_per_seg];
		 int seg_peaks[]=new int[segnum];
		 int first_occur[]=new int[segnum];
		 for(i=0;i<segnum;i++)
		 {
			 int max=hist[i*intensities_per_seg];
			 int val=i*intensities_per_seg;
			 for(j=i*intensities_per_seg+1;j<(i+1)*intensities_per_seg;j++)
			 {
				 if(hist[j]>max)
				 {
					 max=hist[j];
					 val=j;
				 }
			 }
			 seg_peaks[i]=val;
		 }
		 for(i=0;i<intensities_per_seg;i++)
			 code_map[i]=-1;
		 Double temp=Math.log(intensities_per_seg)/Math.log(2);
		 int bits=temp.intValue();
		 //System.out.println("\n"+intensities_per_seg+"			"+bits);
		 for(i=0;i<intensities_per_seg;i++)
		 {
			 Random generator=new Random();
			 j=generator.nextInt(intensities_per_seg);
			 while(code_map[j]!=-1)
				 j=(j+1)%intensities_per_seg;
			 code_map[j]=i;
		 }
		 //System.out.print("code_map-->");
		 //for(i=0;i<intensities_per_seg;i++)
			 //System.out.print(code_map[i]+" ");
		 //System.out.print("\n\nSegment Peaks-->");
		 //for(i=0;i<segnum;i++)
			 //System.out.print(seg_peaks[i]+" ");
		 //while(secret_ptr<secret_len)
		 Outer: for(i=0;i<h;i++)
		 {
			 if(i%2==0)
			 {
				 start=0;
				 inc=1;
			 }
			 else
			 {
				 start=w-1;
				 inc=-1;
			 }
			 for(j=start;;j=j+inc)
			 {
				 if(i%2==0)
				 {
					 if(j>=w)
						 break;
				 }
				 else
				 {
					 if(j<=0)
						 break;
				 }
				 int pix=pixels[i][j];
				 int seg=-1;
				 for(k=0;k<segnum;k++)
				 {
					 if(seg_peaks[k]==pix)
						 seg=k;
				 }
				 if(seg==-1)
					 continue;
				 locmap[i][j]=1;
				 if(first_occur[seg]==0)
				 { 
					 first_occur[seg]=1;
					 continue;
				 }
				int cur_bits=bits-1;
				int dec_num=0;
				while(cur_bits!=-1)
				{
					int ch1=in.read();
					if(ch1==-1)
						break Outer;
					codelength++;
					int ch=(ch1-'0');
					dec_num=dec_num+ch*pwr[cur_bits];
					cur_bits--;
					//secret_ptr++;
					//secret_ptr=((secret_ptr+1)%(secret.length()));
					//secret_len++;
				}
				int new_pix=-1;
				for(k=0;k<intensities_per_seg;k++)
				{
					if(code_map[k]==dec_num)
					{
						new_pix=seg*intensities_per_seg+k;
						out_pixels[i][j]=new_pix;
						break;
					}
				}
			}
		}
		matrix_to_image(out_pixels,"Encoded_image.jpg");
		for(i=0;i<h;i++)
			for(j=0;j<w;j++)
				difference[i][j]=out_pixels[i][j]-pixels[i][j];
		matrix_to_image(difference,"Difference-image.jpg");
		//System.out.println("IMAGE ENCODED WITH SECRET CODE-->done");
		//DECODING STARTS//
		System.out.println("DECRPTION STARTS NOW ...");
		h=out_pixels.length;
		w=out_pixels[0].length;
		int dec_codelength=0;
		int decrpyt_mat[][]=new int[h][w];
		int occur_first[]=new int[segnum];
		int peak_store[]=new int[segnum];
		int intensities_per_segment=256/segnum;
		String hidden_code="";
		System.out.println("h="+h+"and w="+w);
		//int numcodes=0;
		PrintWriter hide = new PrintWriter(new FileWriter("Hidden_Code.txt"));
	    for(i=0;i<h;i++)
		{
			//System.out.println(i+"	:OUTER ...");
				
			if(i%2==0)
			 {
				 start=0;
				 inc=1;
			 }
			 else
			 {
				 start=w-1;
				 inc=-1;
			 }
			 for(j=start;;j=j+inc)
			 {
				 //System.out.println(j+":	INNNER...");
				 if(i%2==0)
				 {
					 if(j>=w)
						 break;
				 }
				 else
				 {
					 if(j<=0)
						 break;
				 }
				 //System.out.println("loc-map value="+locmap[i][j]);
				 if(locmap[i][j]==0){
					 decrpyt_mat[i][j]=out_pixels[i][j];
				 	 continue;}
				 int pix=out_pixels[i][j];
				 int seg=pix/intensities_per_segment;
				 //System.out.println("Segment num="+seg);
				 if(occur_first[seg]==0){
					 occur_first[seg]=1;
					 peak_store[seg]=pix;
					 decrpyt_mat[i][j]=pix;
					 continue;
				 }
				 //System.out.println("reached here...");
				 decrpyt_mat[i][j]=peak_store[seg];
				 int val = code_map[pix%intensities_per_segment];
				 String bin_val=dec_to_bin(val);
				 //System.out.println("Binary value="+bin_val);
				 while(bin_val.length()<bits)
				 {
					 //System.out.println("processing...");
					 bin_val="0"+bin_val;
				 }
				 dec_codelength++;
				 if(dec_codelength!=53034)
					 hide.print(bin_val);
				 //System.out.println("i="+i+"and j="+j+"	>>locmap at lastentry="+locmap[i][j]+",	pixel="+pix+", segment num="+seg+",value="+val);
				 //hide.print(bin_val);
				 //hidden_code=hidden_code+bin_val;
				 //System.out.println("Hidden Code="+hidden_code);
			 }	 
		}
	    System.out.println("Number of chars decrpted=="+dec_codelength);
	    hide.close();
		
	    System.out.println("\nHidden file generated\nEND OF FOR LOOP ...");
		//System.out.println("Number of chars generated ="+numcodes);
		matrix_to_image(decrpyt_mat,"Reconstructed-image.jpg");
		//System.out.println("Hidden Code="+hidden_code+"$%%&%*%**%^\nEXITING.");
		System.out.println("Number of bit="+bits);
		
	}
	 public static String dec_to_bin(int val)
	 {
		 String bin="";
		 do
		 {
			 if(val%2==0)
				 bin="0"+bin;
			 else
				 bin="1"+bin;
			 val=val/2;
		 }
		 while(val>0);
		 return bin;
	 }
}