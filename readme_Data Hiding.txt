  Following work is an implementation of a research paper published in "The Journal of Systems and Software" in the field of Image Processing and Computer Vision.
  TITLE : Histogram-shifting-imitated reversible data hiding 
  AUTHORS : Zhi-Hui Wanga, Chin-Feng Leeb, Ching-Yun Changc
  
  ABSTRACT
	This paper proposes a novel reversible data hiding scheme based on the histogram-shifting-imitated
	approach. Instead of utilizing the peak point of an image histogram, the proposed scheme manipulates
	the peak points of segments based on image intensity. The secret data can be embedded into the cover
	image by changing the peak point pixel value into other pixel value in the same segment. The proposed
	method uses a location map to guarantee the correct extraction of the secret data. Since the modification
	of the pixel value is limited within each segment, the quality of the stego image is only related to the size
	of the segmentation, which means after embedding data into the cover image, it can be reused to do the
	multi-layer data embedding while maintaining the high quality of the final stego image. The experimental
	results of comparison with other existing schemes demonstrate the performance of the proposed scheme
	is superior to the others.
	
	Steps of Code usage :
	1) Extract the rar file.
	2) You will find a folder containing images in which secret code needs to be encrypted and a text file containing secret code.
	3) Modify the code in text file "Code_to_hide.txt" which you want to encrupt.
	4) Run the code "data_hiding.java" selecting the image in which you want to encrypt the given code.
	5) We get the output file contating both encrypted and decrypted images with a file "hidden_code.txt" containing the obtained secret code.
	
	